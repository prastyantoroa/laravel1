@extends('layout.master')
 @section('title')
    Halaman Register    
 @endsection    

 @section('content')           
     
       <h1>Sign Up Form</h1>
    <form action="/daftar" method="POST"  >
        @csrf
        <label> First Name </label><br> 
        <input type="text" name="first_name"/> 
        <br> 
        <label> Last Name </label> 
        <br> 
        <input type="text" name="last_name"/>
        <br>   
        <br>
    
        <label> Gender : </label> 
        <br> 
        <input type="radio" name="male"  >Male
        <br>
        <input type="radio" name="female" checked="checked"/>Female
        <br>
        <input type="radio" name="other"  >Other
        <br> 
    
        <label> Nationality </label> 
        <br> 
        <select name="nationality"><br>
            <option value="indonesian" selected > Indonesian</option>
            <option value="china"> China</option>
            <option value="arab"> Arab</option>
        </select>
        <br> 
        <br>
    
       
        <br>
        <label> Language Spoken</label> 
        <br> 
        <input type="checkbox"checked="checked"/>Bahasa Indonesia
        <br> 
        <input type="checkbox"/>English
        <br> 
        <input type="checkbox"/>Other
        <br>
        <br>
      
        <label> Bio : </label> 
        <br> 
        <textarea name="bio" cols="22" rows="3"></textarea>
        <br> 
        <br>       
       
        <input type="submit" value="Sign Up" >
    </form>

@endsection