@extends('layout.master')
@section('title')
   Tambah Data Cast
@endsection

@section('content')
<div>
    
        <form action="/cast" method="POST">
            @csrf
            <div class="form-group">
                <label for="title">Nama</label>
                <input type="text" class="form-control" name="nama" id="title" placeholder="Masukkan Nama">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">Umur</label>
                <input type="numeric" class="form-control" name="umur" id="title" placeholder="Masukkan umur">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">Bio</label>
                <input type="text" class="form-control" name="bio" id="title" placeholder="Masukkan Bio">
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
      
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>
@endsection